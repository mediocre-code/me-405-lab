# -*- coding: utf-8 -*-
"""@file mcp9808.py
@brief Temp Taking Class
@details Initializes the mcp9808 class. The user will have already created an 
         I2C object and put it into the class, along with the address of the
         I2C device to read from. This class saves that information, then 
         gives back whichever temperature data is asked for. 
@author Jacob Everest
@author Hunter Morse
@date Feb. 10, 2021
@copyright I can't stand this word anymore
"""

import pyb           #import pyb library
from pyb import I2C  #import I2C library from pyb library


class mcp9808:       #create mcp9808 class
    """
    @brief Reads MCP9808 Data
    @details Input a premade object and the address of the I2C device to 
             read from. Call this class and the method of either check,
             celsius, or fahrenheit. Check will return the address of the 
             device, celsius will return the temperature in degrees celsius,
             and fahrenheit will return the temperature in degrees fahrenheit. 
    @author Jacob Everest
    @author Hunter Morse
    @date Feb. 10, 2021
    @copyright I am now refusing to learn what this word means
    """
    def __init__(self, object_name, address): #initialize function, runs at 
                                              #first use of the class, requires
                                              #an object and an address to be
                                              #input as parameters
        """ 
        @brief Initialization Function
        @details Stores the parameters input by the user when creating the 
                 class object so that they may be used later by other 
                 functions.
        @param object_name the name of an I2C object already created by the user
        @param address the location of the I2C device on the nucleo
        @author Hunter Morse
        @author Jacob Everest
        @date Feb. 2, 2021
        """
        self.object_name = object_name      #saves a copy of name parameters as
        self.address = address              #class attributes to be used by
                                            #other functions in the class
    def check(self):                    #just check address of the I2C device
        """ 
        @brief Checks the Location
        @details Checks the location of the I2C device on the nucleo
        @param None
        @author Hunter Morse
        @author Jacob Everest
        @date Feb. 10, 2021
        """
        return self.object_name.scan() #check just scans really
    
    def celsius(self):                 #function to return celsius reading
        """ 
        @brief Measures the temperature in celsius
        @details Reads the data from the MCP9808 in the form of two bytes. It
                 Takes the important sign information from the the bit five of 
                 the first bit, then gets rid of the four MSBs in that bit. It
                 then bit shifts to the left four times on that same byte. It 
                 bit shifts to the right four times on the second byte, then
                 combines the two into one. Then it checks to see if the sign
                 bit was one, thereby declaring the reading to be negative. If
                 it was, then it subratracts the value from 256, if not it does
                 nothing and returns that value as thetemperature.
        @param None
        @author Hunter Morse
        @author Jacob Everest
        @date Feb. 10, 2021
        """
        buf = bytearray([0, 0]) #creating an editable buffer for bytes from mcp9808
        self.object_name.mem_read(buf, self.address, 5) #read from the device
        jack = 0         #random value in which to store a sign flag
        if (buf[0] & 0b00010000):  #if sign flag was set, trigger jack
            jack = 1 #jack is now one
        else:
            pass    #otherwise don't bother
        buf[0] = buf[0] & 0b00001111#get rid of the four MSBs in the first byte
                                    #that was received from the I2C device
        shoot = 16  #standard value for bishifting (four spaces)
        buf[0] = buf[0]*shoot #bitshift first byte left four spaces
        if jack == 1:  #if sign flag was set go here
            temp = -(256 - (buf[0] +buf[1]/shoot)) #subtract value from 256 and
                                                   #make negative, also bitshift
                                                   #the second byte from the 
                                                   #I2C device right 3 places
        else: 
            temp = buf[0]+buf[1]/shoot #do the same bitshifting but no subtracting
        return temp
        
    def fahrenheit(self):   #fahrenheit reading
        """ 
        @brief Measures the temperature in celsius
        @details Reads the data from the MCP9808 in the form of two bytes. It
                 Takes the important sign information from the the bit five of 
                 the first bit, then gets rid of the four MSBs in that bit. It
                 then bit shifts to the left four times on that same byte. It 
                 bit shifts to the right four times on the second byte, then
                 combines the two into one. Then it checks to see if the sign
                 bit was one, thereby declaring the reading to be negative. If
                 it was, then it subratracts the value from 256, if not it does
                 nothing. It converts from celsius to fahrenheit, then it
                 returns that value as thetemperature.
        @param None
        @author Hunter Morse
        @author Jacob Everest
        @date Feb. 10, 2021
        """
        temp = (self.celsius()*9/5)+32 #run through celsius value then convert
        return temp
        
    

