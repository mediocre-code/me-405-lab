"""
@file lab04.py
@brief Nucleo Backend Script
@details This script enables the Nucleo to collect data from its internal
    and connected MCP9808 temperature sensors. Data is collected once
    every minute and written to a .CSV file on the device. Once started 
    the program will run until the user presses ^C. After 12 hours all
    data will be overwritten so that the data file doesn't overflow the 
    chips limited onboard storage.

@author Hunter Morse
@author Jacob Everest
@date Feb 11, 2021
"""

import os
import pyb
from utime import sleep
import mcp9808
from mcp9808 import mcp9808

def getInternalTemp(adcall):
    """
    Grabs the internal temperature on the board
    """
    temp = adcall.read_core_temp();
    return temp


def main():
    """
    Main
    """
	# Create an ADCALL objetct, temp on channel 16
    adcall0 = pyb.ADCAll(12, 0x70000)

    # Specify filename
    filename = 'temps.csv' 
    
    # Specify runtime (suggested = 720)
    max_mins = 720
    
    # Initialize min count
    mins = 0

    # Connect to external sensor
    i2c_object = pyb.I2C(1, pyb.I2C.MASTER, baudrate = 100000)
    mcp9808_object = mcp9808(i2c_object, 24)

    while(True):
        # read add temp vals to csv during specified duration
        if(mins < max_mins):
            with open(filename, "w") as temp_file:
                temp_file.write("Time [min], Temp. Board [deg C], Temp. Sensor [C]\r\n")
                temp_file.write("{:}".format(mins))
                temp = getInternalTemp(adcall0)
                temp = (temp*0.207)+21.477          # Scale internal temp readings
                temp_file.write(", {:}".format(temp))
                i2c_call = mcp9808_object.celsius()
                print(temp)
                print(i2c_call)
                temp_file.write(", {:}\r\n".format(i2c_call))
                sleep(60)
                mins += 1

        # if max mins are exceeded then delete the file and start over
        elif(mins >= max_mins):
            os.remove(filename)
            mins = 0

if __name__ == '__main__':
	main()

