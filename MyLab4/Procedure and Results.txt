Procedure:
Create .py file to run touchy class and call the object_name.allread() (<- there was no name given for this method in the handout
so this is what I called it)
Input length = 180, width = 90, Center of length = 90, Center of width = 45 (arbitrary values for scaling)
Create trace of screen on graph paper
Put axis on graph paper with origin marking center
Place pencil on trace over touchscreen, hold there to mark spot, and run program
Write down X coordinate, Y coordinate, and Z coordinate
Repeat multiple times in different locations

Results:
An image of the results sheet with the trace will be attached. What I learned is that there is an effective zone of 
the touchscreen that does not include the edges (that zone is shown by a dotted line box), that is something I can
say for sure. The rest is difficult to quantify because this was by no means a perfect experiment. The X axis of the screen 
appeared to be shifted in the positive direction from where it should be. Two measurements that should have been on the X axis
read Y values of -0.4 and -1.8. A third, the center location, read 0.2. Considering this is much smaller than the second reading, 
I'll call it an outlier (I know that's bad but it's already an imperfect experimental procedure). The Y axis of the screen 
also shows shifting off of where it should be, but only slightly (2.2 arbitrary units max). Considering the scale of the 
screen, that doesn't seem to bad. The other thing I measured was how long it took the system to return those values (plus the z 
boolean). I took 11 measurements and the average time was 1227.4 microseconds, which is well below the desired value.