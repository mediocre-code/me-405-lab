"""
@file lab04ui.py
@brief User Interface for Lab 04 
@details Reads, parses, and plots data from a .CSV file. For this lab
	only two temperature graphs are plotted, but the functions created
	can be easily modified for other applications

@author Hunter Morse
@author Jacob Everest
@date Feb 11, 2021
"""

import csv
import matplotlib.pyplot as plt
import os.path as path

def plotData(title, x_label, y_label, x1, y1, y1_label = None, x2 = None, y2 = None, y2_label = None):
	"""
	Plot up to two dependent data sets along the same independent
	data set. 
	@param title is the descriptive title of the plot
	@param x_label is the label associated with the x axis
	@param y_label is the label associated with the y axis
	@param x1 is the independent data set assosiated with y1
	@param y1 is the required, first dependent dataset
	@param y1_label is the optional name for the y1 data used in the legend
	@param x2 is the independent data set assosiated with y2
	@param y2 is the optional, second dependent dataset
	@param y2_label is the optional name for the y2 data used in the legend
	"""

	print("Plotting...")
	# plot graph 1
	if(y1_label):
		plt.plot(x1, y1, label = y1_label)
	else:
		plt.plot(x1, y1)
	
	# plot graph 2 if data provided
	if(y2):
		# if missing data or label then raise exception 
		if(not y2_label):
			raise PlotError
		elif(not x2):
			x2 = x1

		plt.plot(x2, y2, label = y2_label)

	# plot labels
	plt.xlabel(x_label)
	plt.ylabel(y_label)
	plt.title(title)
	plt.legend()
	plt.show()

	print('Plotting Complete')

def parseCSV(file, start_line, step = 0, convert = None):
	"""
	Read an open CSV file and return the data in an equivalent matrix
	@param file is an open .CSV file with the data to be read
	@param start_line is the row in the .CSV to begin reading (first line being 0)
	@param step indicates the numebr of lines to skip when reading the file (skip step-1 lines)
	@param convert indicates the desired type of the end matrix ('f' for float)
	"""
	print('Parsing...')
	# line number to implement step
	lineNum = 0
	# Data from csv to be stored as a matrix
	csvData = []
	rowLen = 0

	# read file as csv
	csvFile = csv.reader(file, delimiter = ',')

	# setup csvData matrix
	row = next(csvFile)
	rowLen = len(row)
	csvData = [[] for i in range(rowLen)]
	
	# include first line if not meant to skip
	if(lineNum > start_line):
		for i in range(rowLen):
			if(convert == 'f'):
				csvData[i].append(float(row[i]))
			else:
				csvData[i].append(row[i])

	# increment line count
	lineNum += 1

	# iterate thru remaining lines
	for row in csvFile:
		if(lineNum % step):
			# line to skip
			pass
		else:
			for i in range(rowLen):
				if(convert == 'f'):
					csvData[i].append(float(row[i]))
				else:
					csvData[i].append(row[i])

		lineNum += 1

	# remove lines before the start line
	for col in range(rowLen):
		csvData[col] = csvData[col][start_line:]

	print('Parsing Complete')

	return csvData

def main():
	filename = 'temps.csv'
	title = 'Ambient and MCU Internal Temperatures vs. Time'
	xLabel = 'Time [s]'
	yLabel = 'Temperature [C]'
	y1Label = 'MCU Temp.'
	y2Label = 'Ambient Temp.'
	data = []

	try:
		if path.exists(filename):
			with open(filename, 'r') as temps:
				# parse the csv file skipping the first two lines, 
				# skipping every other line after that, and 
				# converting all data to floats
				data = parseCSV(temps, 2, 2, 'f')
			plotData(title, 			# plot title
				xLabel, 				# x-axis label
				yLabel, 				# y-axis label
				data[0], 				# x1 data
				data[1], 				# y1 data
				y1Label, 				# y1 data label
				y2 = data[2], 			# y2 data
				y2_label = y2Label)		# y2 data label

		else:
			raise PathError


	except KeyboardInterrupt:
		# If ^C pressed stop the program
		print('Leaving so soon?')
		print('Goodbye!')

	except PlotError:
		# Error plotting occured
		print('An error occurred while plotting your data. Please \n\
			verify everything looks right')

	except PathError:
		# Error finding file at path
		print('File not found')


if __name__ == '__main__':
	main()






