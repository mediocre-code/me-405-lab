# -*- coding: utf-8 -*-
"""@file touchy.py
@brief Placement on touchscreen
@details Initializes a touchy class. Takes readings of where the attached 
         touchscreen is being touched. Returns values for x location, y
         location, and whether or not the screen is even being touched. Average
         time to run the method that retreived the x coordinate, y coordinate,
         and z boolean ended up being about 1227 microseconds, well bellow the
         given threshold of 1500 for all three. I will attach a picture of my
         setup and an example of the putty window where I got my results. In 
         the picture there are four outputs: the first is the x coordinate of
         where the screen is being touched, the second is the y coordinate of 
         where the screen is being touched, the third is the z boolean which 
         tells whether or not the screen is being touched, and the fourth is a
         tuple that presents the x coordinate, y coordinate, z coordinate, and 
         the overall timing that it took to take those three measurements (in
         microseconds).
@author Jacob Everest
@date Mar. 1, 2021
@copyright I outright protest this word
"""

import pyb
import utime

class touchy:
    """
    @brief Tells where it is being touched
    @details Input 4 pin values, the length of the touchscreen, the width of
             the touchscreen, the distance from x = 0 edge to the center, and 
             the distance form the y = 0 edge to the center. Once given, the 
             class will return results from whichever method activated. 
    @author Jacob Everest
    @date Mar. 1, 2021
    @copyright I am now giving this word the silent treatment
    """
    def __init__ (self, Xmpin, Xppin, Ympin, Yppin, Length, Width, X0, Y0):
        """ 
        @brief Initialization Function
        @details Stores the parameters input by the user when creating the 
                 class object so that they may be used later by other 
                 functions.
        @param Xmpin Value of the pin that the user wants to use for Xm
        @param Xppin Value of the pin that the user wants to use for Xp
        @param Ympin Value of the pin that the user wants to use for Ym
        @param Yppin Value of the pin that the user wants to use for Yp
        @param Length Length of the touchscreen the user has
        @param Width Width of the touchscreen the user has
        @param X0 X distance from the center of the touchscreen to the origin
        @param Y0 Y distance from the center of the touchscreen to the origin
        @author Jacob Everest
        @date Mar. 1, 2021
        """
        ##@brief Low X pin
        # @details This pin is kept floating in order to read values from the high X
        self.Xmpin = Xmpin    #save parameter for later use
        
        ##@brief High X pin
        # @details This pin is high to output a voltage that will go through
        #          the touchscreen
        self.Xppin = Xppin    #save parameter for later use
        
        ##@brief Low Y pin
        # @details This pin is kept floating in order to read values from the high Y
        self.Ympin = Ympin    #save parameter for later use
        
        ##@brief High Y pin
        # @details This pin is high to output a voltage that will go through
        #          the touchscreen
        self.Yppin = Yppin    #save parameter for later use
        
        ##@brief Low X pin object
        # @details Set the Pin object as an input to begin
        self.Xm = pyb.Pin(Xmpin, pyb.Pin.IN)    #set as input to begin with
        
        ##@brief High X pin object
        # @details Set the Pin object as an input to begin
        self.Xp = pyb.Pin(Xppin, pyb.Pin.IN)    #set as input to begin with
        
        ##@brief Low Y pin object
        # @details Set the Pin object as an input to begin
        self.Ym = pyb.Pin(Ympin, pyb.Pin.IN)    #set as input to begin with
        
        ##@brief High Y pin object
        # @details Set the Pin object as an input to begin
        self.Yp = pyb.Pin(Yppin, pyb.Pin.IN)    #set as input to begin with
        
        ##@brief Length value
        # @details Stores the value of the length of the screen that the user input
        self.Length = Length    #save the length value for later use
        
        ##@brief Width value
        # @details Stores the value of the width of the screen that the user input
        self.Width = Width    #save width value for later use
        
        ##@brief Center X value
        # @details Stores the value of the X center of the screen that the user input
        self.X0 = X0    #save x value of the center of the screen for later use
    
        ##@brief Center Y value
        # @details Stores the value of the Y center of the screen that the user input
        self.Y0 = Y0    #save y value of the center of the screen for later use
        
    def Xcoord(self):
        """ 
        @brief X-coordinate method
        @details Reads the value of the Ym pin to determine the voltage being
                 sent to it by contact with the Xp Xm line. This voltage is 
                 proportional to the distance along the x axis where the screen 
                 is being touched. That is converted from an analog ot a 
                 digital value between 0 and 4095. The value is then divided
                 by 4095 and multiplied by the length of the screen to get the
                 actual x placement of where the screen is being touched. X0 is
                 then subtracted from that value to put the origin at the 
                 center of the screen lengthwise, with the negative being on 
                 the left and the positive being on the right.
        @param None
        @author Jacob Everest
        @date Mar. 1, 2021
        """
        self.Xm = self.Xmpin.init(mode = pyb.Pin.OUT_PP, value = 0) #set pin
        self.Xp = self.Xppin.init(mode = pyb.Pin.OUT_PP, value = 1) #set pin
        self.Ym = self.Ympin.init(mode = pyb.Pin.IN) #set pin 
        self.Yp = self.Yppin.init(mode = pyb.Pin.IN) #set floating pin
        
        ##@brief YM reader object
        # @details Reads the value coming from the X high pin when connected
        Ymreader = pyb.ADC(self.Ympin)    #turn Ympin into ADC 
        
        pyb.udelay(3) #delay some time to settle
        
        ##@brief Xvoltage
        # @details Reads value from YM reader
        Xvolts = Ymreader.read()    #read value of the Ympin and save
        
        ##@brief X distance Value
        # @details Computes value of the x distance where screen is touched
        Xdist = ((Xvolts/4095)*self.Length) - self.X0 #convert saved to dist.
        return Xdist #return x distance value
    
    def Ycoord(self):
        """ 
        @brief Y-coordinate method
        @details Reads the value of the Xm pin to determine the voltage being
                 sent to it by contact with the Yp Ym line. This voltage is 
                 proportional to the distance along the y axis where the screen 
                 is being touched. That is converted from an analog ot a 
                 digital value between 0 and 4095. The value is then divided
                 by 4095 and multiplied by the width of the screen to get the
                 actual x placement of where the screen is being touched. Y0 is
                 then subtracted from that value to put the origin at the 
                 center of the screen widthwise, with the negative being on 
                 the bottom and the positive being on the top.
        @param None
        @author Jacob Everest
        @date Mar. 1, 2021
        """
        self.Ym = self.Ympin.init(mode = pyb.Pin.OUT_PP, value = 0) #set pin
        self.Yp = self.Yppin.init(mode = pyb.Pin.OUT_PP, value = 1) #set pin
        self.Xm = self.Xmpin.init(mode = pyb.Pin.IN) #set pin
        self.Xp = self.Xppin.init(mode = pyb.Pin.IN) #set floating pin
        
        ##@brief XM reader object
        # @details Reads the value coming from the Y high pin when connected
        Xmreader = pyb.ADC(self.Xmpin)    #turn Xmpin into ADC
        pyb.udelay(3) #delay for settling time
        
        ##@brief Yvoltage
        # @details Reads value from XM reader
        Yvolts = Xmreader.read()    #read value of Xmpin and save
        
        ##@brief X distance Value
        # @details Computes value of the x distance where screen is touched
        Ydist = ((Yvolts/4095)*self.Width) - self.Y0 #convert saved to dist.
        return Ydist #return y distance value
    
    def Zbool(self):
        """ 
        @brief Z-coordinate method
        @details Reads the value of the Ym pin to determine the voltage being
                 sent to it by contact with the Yp Xm line. When the screen is
                 being touched, Ym will receive a lower amount of voltage as
                 some of the voltage will go to Xm which is set to low. When
                 the value being read by Ym decreases, Z boolean pin will be
                 set to say that contact has been made.
        @param None
        @author Jacob Everest
        @date Mar. 1, 2021
        """
        self.Xm = self.Xmpin.init(mode = pyb.Pin.OUT_PP, value = 0) #set pin
        self.Yp = self.Yppin.init(mode = pyb.Pin.OUT_PP, value = 1) #set pin
        self.Ym = self.Ympin.init(mode = pyb.Pin.IN) #set pin
        self.Xp = self.Xppin.init(mode = pyb.Pin.IN) #set floating pin
        ##@brief Z reader object
        # @details Reads the value coming from the Y high pin when connected
        Zreader = pyb.ADC(self.Ympin)    #turn Ympin into ADC
        
        pyb.udelay(3) #delay for settling
        
        ##@brief Z voltage
        # @details Reads value from XM reader
        Zvolts = Zreader.read()    #read value of Xmpin and save
        
        if (200 < Zvolts < 3900): #see if voltage is b/w extremes 
            ##@brief Z boolean
            # @details indicates whether screen is touched or not
            Zbool = 1 #if between extremes, screen is being touched
        else:
            Zbool = 0 #if at extremes, screen is not being touched
        return Zbool #return boolean as 1 if being touched, 0 if not
    
    def allread(self):
        """ 
        @brief Read all coordinates
        @details Runs the x coordinate, y coordinate, and z boolean methods. It
                 then returns those values in a tuple. It also times how long 
                 it takes itself to do that in microseconds. 
        @param None
        @author Jacob Everest
        @date Mar. 1, 2021
        """
        ##@brief Timer start
        # @details starts timing how long this method takes
        start = utime.ticks_us() 
        
        ##@brief X coordinate
        # @details Saves the X coordinate we read
        ni = self.Xcoord() #run Xcoord method and save as a dreaded sound
        
        ##@brief Y coordinate
        # @details Saves the Y coordinate we read
        shrubbery = self.Ycoord() #run Ycoord method and save as a desired plant
        
        ##@brief Z boolean
        # @details Saves the Z boolean indicator
        holy_handgrenade = self.Zbool() #run Zbool method and save as a bunny fighting weapon
        
        ##@brief Timer End
        # @details Stops timing how long this method takes
        end = utime.ticks_us() #stop the timer that tracked how long measurements took
        
        ##@brief Values list
        # @details Compiles a list of all things we obtained in this method.
        ikki_ikki = (ni, shrubbery, holy_handgrenade, end-start) #compile a list of
        #important sounds, objects, and timing and save as the dreaded sound of the knights who 
        #formerly said ni
        return ikki_ikki #return said sound
        
        
        
        