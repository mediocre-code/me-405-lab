## @file mainpage.py
# Documentation for / use of mainpage.py
#
# Detailed doc for mainpage.py
#
# @mainpage
#
# @section sec_port Portfolio Details
# This is my ME405 Portfolio. See individual modules for detals.
#
# Included modules are:
# * Lab0x01 (\ref sec_lab1)
# * Lab0x02 (\ref sec_lab2)
# * Lab0x03 (\ref sec_lab3)
# * Lab0x04 (\ref sec_lab4)
# * Lab0x05 (\ref sec_lab5)
# * Lab0x06 (\ref sec_lab6)
# * Lab0x07 (\ref sec_lab7)
#
# @section sec_lab1 Lab0x01 Documentation
# * Source: https://bitbucket.org/SubparCoder/subparcode/src/master/Lab1/Lab1.py
# * Documentation: \ref sec_lab1
#
# @section sec_lab2 Lab0x02 Documentation
# * Source: https://bitbucket.org/SubparCoder/subparcode/src/master/Lab2/Lab2.py
# * Documentation: \ref Lab2
#
# @section sec_lab3 Lab0x03 Documentation
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/Lab3/Lab3fe.py
# * Documentation: \ref Lab3fe
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/Lab3/Lab3be.py
# * Documentation: \ref Lab3be
#
# @section sec_lab4 Lab0x04 Documentation
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/MyLab4/Lab4.py
# * Documentation: \ref Lab4
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/MyLab4/Lab4User.py
# * Documentation: \ref Lab4User
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/MyLab4/mcp9808.py
# * Documentation: \ref mcp9808
#
# @section sec_lab5 Lab0x05 Documentation
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/Lab5/Lab6.pdf
# * Documentation: \ref Lab5
#
# @section sec_lab6 Lab0x06 Documentation
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/Lab6/
# * Documentation: \ref Lab6
#
# @section sec_lab7 Lab0x07 Documentation
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/Lab7/touchy.py
# * Documentation: \ref Lab7
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/Lab7/Procedure%20and%20Results.txt
# * Documentation: \ref Procedure and Results
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/Lab7/TestSetup.jpg
# * Documentation: \ref Test Setup
# * Source: https://bitbucket.org/mediocre-code/me-405-lab/src/master/Lab7/Test%20Results.jpg
# * Documentation: \ref Test Results
#
# @author Jacob Everest
#
# @date Jan. 14, 2021
