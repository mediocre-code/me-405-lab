# -*- coding: utf-8 -*-
"""@file mainWithnFault.py
@brief Another attempt to balance the table
@details As with mainWithnFault.py, this program tried to balance the table, 
         except this one actually worked (for small displacements)! This ran
         off a combination of the two concepts from mainWithnFault.py. It had
         a range of values for duty cycle based on how far the encoder was from
         where we wanted it to be. However, it was not a "continuous" spectrum. 
         Instead, there would be a range of distances that would share the same
         duty cycle. As the encoder got closer to where we wanted it to be, the
         duty cycle would gradually step down at set distances. This allowed
         us to more directly control the duty cycles and tune the system until
         it worked, which it did! Also here we changed the range of values 
         from 0-65535 to 0-255, as that is a bit easier to work with.
@author Jacob Everest
@author Hunter Morse
@date Mar. 15, 2021
@copyright This document shall only be viewed by those who will give us an A
"""

import MotorDriver
from MotorDriver import MotorDriver
import pyb
import EncoderDriver
from EncoderDriver import EncoderDriver
import touchy2
from touchy2 import touchy
##@brief X axis motor
# @details Creates an object that controls the motor that pushes the table 
#          Around its widthwise axis
var = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB4, pyb.Pin.board.PB5, 3, 1, 2)
##@brief Y axis motor
# @details Creates an object that controls the motor that pushes the table 
#          Around its lengthwise axis
var2 = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB0, pyb.Pin.board.PB1, 3, 3, 4)
#create MotorDriver object
##@brief X axis Motor encoder
# @details Encoder object that outputs position of the X axis motor
balancer = EncoderDriver(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 1, 2, pyb.Timer(4, prescaler = 7, period = 0xff))
##@brief Where to begin
# @details Where we want the motors to shoot for
beginner = 0
balancer.setPosition(beginner)
##@brief Y axis Motor encoder
# @details Encoder object that outputs position of the Y axis motor
balancer2 = EncoderDriver(pyb.Pin.board.PC6, pyb.Pin.board.PC7, 1, 2, pyb.Timer(8, prescaler = 7, period = 0xff))
balancer2.setPosition(beginner)
##@brief Fault pin
# @details Pin that the faults are dependent upon
fault = pyb.Pin.board.PB2 #fault pin on the nucleo
def fault_isr (fault): #fault isr to be triggered when bad juju happens
    """
    @brief Fault ISR
    @details Sets the faultflag to one if a fault isr has been triggered. Stops
             all functionality until resolved.
    @author Jacob Everest
    @author Hunter Morse
    @date Mar. 15, 2021
    @copyright No
    """
    global beginner #use fault flag in here
    beginner = beginner - balancer2.getPosition() #set flag to 1 to indicate a fault has occured
    balancer2.setPosition(beginner) 
##@brief External Interrupt Object
# @details Allows the Fault interrupt to be called    
extint = pyb.ExtInt(pyb.Pin.board.PB2, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, fault_isr)
#external interrup to be used when current draw to PB2 is too high
var.enable()
while True: 
    if -128 < balancer.balanceDif() <= -40: #when the X motor is on the right
    #side (if you're looking directly down the shaft), we set duty to be
    #negative so it spins counter clockwise
        var.set_duty(-80)
    elif -40 < balancer.balanceDif() <= -20:
        var.set_duty(-80)
    elif -20 < balancer.balanceDif() <= -5:
        var.set_duty(-50)
    elif -5 < balancer.balanceDif() < -1:
        var.set_duty(-30)    
    elif -1 <= balancer.balanceDif() :
        var.stop()
    if -215 < balancer.balanceDif() <= -128: #when the X motor is on the left
    #side (if you're looking directly down the shaft), we set duty to be
    #positive so it spins clockwise
        var.set_duty(90)
    elif -235 < balancer.balanceDif() <= -215:
        var.set_duty(99)
    elif -250 < balancer.balanceDif() <= -235:
        var.set_duty(90)
    elif -256 < balancer.balanceDif() <= -250:
        var.set_duty(80)
        
    if -128 < balancer2.balanceDif() <= -40: #when the Y motor is on the right
    #side (if you're looking directly down the shaft), we set duty to be
    #Negative so it spins counter clockwise
        var2.set_duty(-80)
    elif -40 < balancer2.balanceDif() <= -20:
        var2.set_duty(-80)
    elif -20 < balancer2.balanceDif() <= -5:
        var2.set_duty(-50)
    elif -5 < balancer2.balanceDif() < -1:
        var2.set_duty(-30)    
    elif -1 <= balancer2.balanceDif() :
        var2.stop()
    if -215 < balancer2.balanceDif() <= -128: #when the Y motor is on the left
    #side (if you're looking directly down the shaft), we set duty to be
    #positive so it spins clockwise
        var2.set_duty(90)
    elif -235 < balancer2.balanceDif() <= -215:
        var2.set_duty(99)
    elif -250 < balancer2.balanceDif() <= -235:
        var2.set_duty(90)
    elif -256 < balancer2.balanceDif() <= -250:
        var2.set_duty(70)
    print(balancer2.getPosition())