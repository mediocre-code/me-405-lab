# -*- coding: utf-8 -*-
"""@file BalancingFunctionalityAttempt.py
@brief Attempt to balance the ball
@details Once the table was essentially balanced, it was time to try to balance
         the ball. However, at this point Jacob realized that something was broken
         with my touch pad setup. It was the exact same code he used for lab 7
         which worked like a charm, and Hunter and Jacob swapped screens and it
         still didn't work, so we figured it must've been the wiring. For that
         Reason Jacob could not really test this code. It is based on the 
         same concept as the initial goal for the balancing proportionality. 
         The farther the ball is away from the origin the higher the duty
         cycle. Also, the speed of the ball changes that too. Duty cycle calc
         multiplies them by the gains we found from analysis, adds them together
         and applies that duty to the motor. So if the ball is on the right 
         side but rolling towards the center, the platform will begin to settle
         back down towards being flat. This was an attempt at a proportional 
         plus derivative controller. 
@author Jacob Everest
@author Hunter Morse
@date Mar. 15, 2021
@copyright This document shall only be viewed by those who will give us an A
"""

import MotorDriver
from MotorDriver import MotorDriver
import pyb
import EncoderDriver
from EncoderDriver import EncoderDriver
import touchy2
from touchy2 import touchy
#not working
##@brief X axis motor
# @details Creates an object that controls the motor that pushes the table 
#          Around its widthwise axis
var = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB4, pyb.Pin.board.PB5, 3, 1, 2)
#controls the y
#create MotorDriver object
##@brief X axis Motor encoder
# @details Encoder object that outputs position of the X axis motor
var2 = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB0, pyb.Pin.board.PB1, 3, 3, 4)
#controls the x
##@brief Touchy pin
# @details Touchy object to read where the touch screen is being touch
touch = touchy(pyb.Pin.board.PA1, pyb.Pin.board.PA7, pyb.Pin.board.PA0, pyb.Pin.board.PA6, 400, 200, 270, 100)
##@brief previous length
# @details Where the ball was along the length of the screen during the past
#          reading
lengthpast = 0
##@brief previous width
# @details Where the ball was along the width of the screen during the past
#          reading
widthpast = 0
##@brief X motor duty cycle
# @details Slightly confusing, but it pushes or puls the y axis to a new angle
yduty = 0
##@brief Y motor duty cycle
# @details Slightly confusing, but it pushes or puls the x axis to a new angle
xduty = 0
var.enable()
while True:
    spot = touch.allread()
    onoff = spot[2]
    if onoff == 1:
        length = -spot[0]
        ##@brief Speed of the ball
        # @details Speed with which the ball is moving along the length of the
        #          screen. As it increases, so does the duty cycle
        lspeed = lengthpast-length
        lengthpast = length
        width = -spot[1]
        ##@brief Speed of the ball
        # @details Speed with which the ball is moving along the width of the
        #          screen. As it increases, so does the duty cycle
        wspeed = widthpast-width
        widthpast = width
        xduty = (2.15*length*80/200) + (7.55*lspeed*20/200)
        yduty = (2.15*width*50/100) + (7.55*wspeed*50/100)
        var2.set_duty(xduty)
        var.set_duty(yduty)
        print(spot)
    else:
        var.stop()
        var2.stop()