# -*- coding: utf-8 -*-
"""@file MotorDriver.py
@brief Runs the Motor
@details Initializes a motor object when used by the user. This can be done with
         multiple different pin values in order to run more than one motor at
         a time. It uses timer channel objects and pulse width modulation to 
         run the motors connected to the right pins at varying speeds. These 
         pins can be attached to the same timer number, but must have different
         channels on that timer. Thus, they can run independently of each other.
@author Jacob Everest
@author Hunter Morse
@date Mar. 9, 2021
@copyright None
"""
import pyb #import Pyboard class

class MotorDriver:
    ''' 
    @brief Motor Driver Class
    @details This class implements a motor driver for the
             ME405 board. 
    @author Hunter Morse
    @author Jacob Everest
    @date Mar. 9, 2021
    @copyright No
    '''
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, IN1Pin_channel, IN2Pin_channel):
        ''' 
        @brief Initializes the Motor Driver class
        @details Creates a motor driver by initializing GPIO pins and turning
                 the motor off for safety. Stores values as self.variables in
                 order to be used in different parts of the class.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on
               IN1_pin and IN2_pin. 
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        '''
        ##@brief Enable pin name
        # @details pyb.Pin object variable in which we initialize the nSleep_pin
        #          and set it up as an output.
        self.grouch = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        
        ##@brief Clockwise pin object
        # @details pyb.Pin object variable in which we initialize the IN1_pin
        #          as an output
        CW = pyb.Pin(IN1_pin, pyb.Pin.OUT_PP)
        
        ##@brief Counterclockwise pin object
        # @details pyb.Pin object variable in which we initialize the IN2_pin
        #          as an output
        cCW = pyb.Pin(IN2_pin, pyb.Pin.OUT_PP)
        
        ##@brief Timer object
        # @details pyb.Timer object variable in which we use the timer number 
        #          and set the frequency
        tim = pyb.Timer(timer, freq=20000)
        
        ##@brief Clockwise Channel
        # @details channel object that uses the IN1Pin_channel from the parameters
        #          and the CW from earlier, while also setting up PWM
        self.clockwise = tim.channel(IN1Pin_channel, pyb.Timer.PWM, pin=CW)
        
        ##@brief Counterclockwise Channel
        # @details channel object that uses the IN2Pin_channel from the parameters
        #          and the cCW from earlier, while also setting up PWM
        self.cclockwise = tim.channel(IN2Pin_channel, pyb.Timer.PWM, pin=cCW)
        
        print ('Creating a motor driver')

    def enable (self):
        ''' 
        @brief Enabling Method
        @details Sets the grouch object (enable pin) to high in order to allow
                 all motors connect to it to run.
        @param None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        '''
        self.grouch.high() #set pin high
        print ('Enabling Motor')

    def disable (self):
        ''' 
        @brief Disabling method
        @details Sets the grouch object (enable pin) to low to stop the all
                 motors connected to the pin (stop output to them at least,
                 they can still coast or be pushed)
        @param None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        '''
        self.grouch.low() #set pin low
        print ('Disabling Motor')

    def set_duty (self, duty):
        ''' 
        @brief Duty input
        @details This method sets the duty cycle to be sent
                 to the motor to the given level. Positive values
                 cause effort in one direction, negative values
                 in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal
                    sent to the motor 
        @author Hunter Morse 
        @author Jacob Everest
        @date Mar. 9, 2021
        '''
        if duty > 0: #if positive duty
            self.cclockwise.pulse_width_percent(0) #set cclockwise PWM to zero
            self.clockwise.pulse_width_percent(duty) #set clockwise PWM to run
            #at the duty cycle input
            print( "Forwards") 
        else:#if duty < 0:
            self.clockwise.pulse_width_percent(0) #set clockwise PWM to zero
            
            ##@brief Duty Cycle
            # @details Saves the duty cycle enter multiplied by negative one
            #          so that it is positive
            duty = duty*(-1)
            
            self.cclockwise.pulse_width_percent(duty) #set cclockwise PWM to
            #run at the absolute value of the duty cycle input
            print('Backwards')
            
    def stop(self):
        ''' 
        @brief Stop Method
        @details This method sets the duty cycle to be sent
                 to the motor as 90 in both directions, therefore keeping the 
                 motor exactly where it is.
        @param None
        @author Hunter Morse 
        @author Jacob Everest
        @date Mar. 9, 2021
        '''        
        self.cclockwise.pulse_width_percent(90) #push and pull in opposite 
        self.clockwise.pulse_width_percent(90) #directions to keep it stopped
        

