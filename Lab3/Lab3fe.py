# -*- coding: utf-8 -*-
"""@file Lab3fe.py
@brief Button Step
@details This side of the code asks you to press g, then sends that off to the
         back end. It then waits for the back end to send values back and 
         then ideally it would split them up and plot them. Unfortunately, 
         because my microcontroller was broken for 4 of the 7 days we had to 
         do this, thats not exactly happening. Nevertheless, I plan to work on
         this more and get it working at some point, even if it's not graded
@author Jacob Everest
@date Jan. 28, 2021
@copyright I refuse to look up what this word means
"""

import serial                 #bring in the serial library
from matplotlib import pyplot #library for plotting
ser = serial.Serial(port='COM4',baudrate=115200,timeout=5) #open serial port
_col1 = list()                 #create lists to store values later
_col2 = list()
_math = 500                    #number of data points we want to catch


def sendChar():
    """ 
    @brief G character
    @details asks you to press a character then sends that character to the 
             microcontroller. At this point it does not work and I know that. 
             however, it is late, I am getting unreasonably frustrated, and
             not making any progress in this state. Hopefully I can scrape up
             points for organization, documentation, and submission now, and 
             then I will make it work later
    @param None
    @author Jacob Everest
    @date Jan. 28, 2021
    """
    inv = input('Please Type g Then {Enter}: ') #create intro message and wait
    ser.write(str(inv).encode('ascii'))         #send entered value to micro.

for n in range(1):            #run through this process once
    ser.write(b"\x03")        #control c sent to stop the last run
    ser.write(b"\x04")        #condrol d sent to soft reboot
    arraying = sendChar()     #just call sendChar function

   # myRows = ser.readline().decode('ascii')  #read serial port and collect values
    #print(myRows)             #print it for checking
   # ser.close()               #close the serial port to access it on another run
    for count in range(_math):      #iterate through each row
        try:
            myRows = ser.readline().decode('ascii')  #read serial port and collect values
            myList = myRows.strip('\r\n').split(';') #take away the \n and \r
            #from each row and then break up the numbers into two strings       
            _col1.append(int(myList[0])) #add first value from each row to this
            _col2.append(int(myList[1])) #add second value from each row to this
        except:
            _math = 500
        

ser.close()               #close the serial port to access it on another run

          
pyplot.figure()      #create the figure
pyplot.plot(_col1, _col2) #populate the figure with x and y values
pyplot.ylabel('Voltage Level') #label y axis as voltage output
pyplot.xlabel('Time [us]')     #label x axis as the time in microseconds
pyplot.title("Voltage Level After Button Press") #title
