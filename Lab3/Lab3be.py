# -*- coding: utf-8 -*-
"""@file Lab3be.py
@brief Button Step
@details Program initializes and waits for g to be pressed. Once it is, you
         have a few seconds to press the blue button. This part of the code
         reads the value of the button voltage and prints it out into a buffer 
         then sends it away
@author Jacob Everest
@date Jan. 28, 2021
@copyright I will never know what this word means
"""

import pyb    #import pyb library
import array  #import array library
from pyb import UART #import UART specifically
_myuart = UART(2)    #create uart variable
_adc = pyb.ADC(pyb.Pin.board.A0) #set pin A0 to read analog values and convert
                                 #them to digital ones
_timing = 100000                 #amount of readings we take per second
_step = 10                       #time step
_buffy = array.array('H', (0 for index in range (500))) #buffer to hold voltage values
_timy = array.array('H', [0 + _step*interval for interval in range (500)]) #buffer for time
   
_tim = pyb.Timer(2)      # create a timer object using timer 2 
_tim.init(freq = _timing) #initialize the timer and set the frequency from above
_adc.read_timed(_buffy, _tim) #read values from A0 and store them when tim overflows

while True:              #wait here until g is pressed
    if _myuart.any() == 0: #if nothing is pressed, don't move
        pass
    else:
        value = _myuart.readchar() #if something is pressed, store it
        if value != 103:     #if the value stored is not g, ignore it
            pass
        else:         #if the value stored was g, move on
            break      
        
while ((_buffy[-1]-_buffy[-500])<4000) or ((_buffy[-1]-_buffy[-5])>10) or _buffy[150] == 0:
#above logic is something I'm proud of: if the first and last value in buffy are
#not at least 4000 away from each other, keep taking values. If the last and fifth
#from last values are more than 10 away from each other, keep taking values. and if
#the 150th value in buffy is zero, keep taking value. This means it waits until it has
#gotten high enough, waits until it has flattened out enough, and waits until the graph
#has centered    
    _adc.read_timed(_buffy, _tim)
for n in range(len(_timy)):   #for as long as buffer timy is
    _myuart.write('{:}; {:}\r\n'.format (_timy[n], _buffy[n]))
    #write values to the uart to be read by front end
    
    
    

