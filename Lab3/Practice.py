# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 09:27:50 2021

@author: jacob
"""

import serial
ser = serial.Serial(port='COM4', baudrate = 115200, timeout = 1)
def sendChar():
    inv = input('Press g-Enter to begin: ')
    ser.write(str(inv).encode('ascii'))
    myvalue = ser.readline().decode('ascii')
    return myvalue    
for n in range(1):
    print(sendChar())
        
ser.close()

