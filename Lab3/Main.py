# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 10:01:25 2021

@author: jacob
"""

from pyb import UART
myuart = UART(2)
while True:
    if myuart.any() != 0:
        val = myuart.readchar()
        myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo')